const Task = require("../models/taskShema");

// Retrieving all tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result;
	});
};

// Creating a task
module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name,
	});
	return newTask.save().then((task, err) => {
		if (err) {
			console.log(err);
			return false;
		} else {
			return task;
		}
	});
};

// Delete tasks
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if (err) {
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	});
};

// Update tasks
module.exports.updateTask = (taskId, reqBody) => {
	return Task.findById(taskId).then((result, err) => {
		if (err) {
			console.log(err);
			return false;
		} else {
			result.name = reqBody.name;
			return result.save().then((updatedTask, saveErr) => {
				if (saveErr) {
					console.log(saveErr);
					return false;
				} else {
					return updatedTask;
				}
			});
		}
	});
};

// ACTIVITY ============================================
module.exports.getSpecificTasks = (taskId) => {
	return Task.findById(taskId).then((result) => {
		return result;
	});
};

module.exports.updateTaskStatus = (taskId, reqBody) => {
	return Task.findById(taskId).then((result, err) => {
		if (err) {
			console.log(err);
			return false;
		} else {
			result.status = reqBody.status;
			return result.save().then((updatedStatus, saveErr) => {
				if (saveErr) {
					console.log(saveErr);
					return false;
				} else {
					return updatedStatus;
				}
			});
		}
	});
};
