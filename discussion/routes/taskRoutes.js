const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskControllers");

// Retrieve tasks
router.get("/", (req, res) => {
	taskController
		.getAllTasks()
		.then((resultFromController) => res.send(resultFromController));
});

// Create tasks
router.post("/createTask", (req, res) => {
	taskController
		.createTask(req.body)
		.then((resultFromController) => res.send(resultFromController));
});

// Delete tasks
router.delete("/deleteTask/:id", (req, res) => {
	taskController
		.deleteTask(req.params.id)
		.then((resultFromController) => res.send(resultFromController));
});

// Update tasks
router.put("/updateTask/:id", (req, res) => {
	taskController
		.updateTask(req.params.id, req.body)
		.then((resultFromController) => res.send(resultFromController));
});

// ACTIVITY =======================================
router.get("/:id", (req, res) => {
	taskController
		.getSpecificTasks(req.params.id)
		.then((resultFromController) => res.send(resultFromController));
});

router.put("/:id/complete", (req, res) => {
	taskController
		.updateTaskStatus(req.params.id, req.body)
		.then((resultFromController) => res.send(resultFromController));
});
module.exports = router;
