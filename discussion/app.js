const express = require("express");
const app = express();
const port = 4000;
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes");

// Connecting to MongoDB
mongoose.connect(
	"mongodb+srv://admin:admin@b145.m0v7t.mongodb.net/session33?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
);

let db = mongoose.connection;
db.on(
	"error",
	console.error.bind(console, "There is an error with the connection")
);
db.once("open", () => console.log(`Successfully connected to the database`));

// Middelwares
app.use(express.json());
app.use(
	express.urlencoded({
		extended: true,
	})
);

app.use("/tasks", taskRoutes);
app.listen(port, () => console.log(`Server running at port ${port}`));
