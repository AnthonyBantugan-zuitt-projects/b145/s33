const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name of task is required"],
	},
	status: {
		type: String,
		default: "pending",
	},
	createdOn: {
		type: Date,
		default: new Date(),
	},
});

module.exports = mongoose.model("Task", taskSchema);
